#!/bin/sh -

set -euf

needs_cleanup=false

sanity_checks()
{
	info 'Running sanity checks'

	# Environment:
	if [ "$(id -u)" = '0' ]; then
		workdir_prefix=/run/cucumber
		readonly LISTFILE=/etc/cucumber.list
	else
		test -n "$XDG_RUNTIME_DIR" || die $E_ENV 'XDG_RUNTIME_DIR is not set'
		workdir_prefix=$XDG_RUNTIME_DIR/cucumber
		readonly LISTFILE=${XDG_CONFIG_HOME:-~/.config}/cucumber.list
	fi

	# Output file and working directory:
	readonly ARCHIVE=$(realpath "$ARCHIVE")
	if ! $FORCE; then
		test ! -e "$ARCHIVE" \
			|| die $E_FILESYSTEM '%s: file or directory exists' "$ARCHIVE"
	fi
	readonly WORKDIR=$workdir_prefix$ARCHIVE
}

create_workdir()
{
	info 'Creating working directory'

	mkdir -p "${WORKDIR%/*}" || die $E_FILESYSTEM
	mkdir "$WORKDIR" \
		|| die $E_FILESYSTEM \
		       'Another instance of %s may be running' "$PROGRAM_NAME"

	needs_cleanup=true
}

copy_data()
{
	info 'Copying data to working directory'

	test -r "$LISTFILE" \
		|| die $E_CONFIG '%s: file not found or not readable' "$LISTFILE"

	# shellcheck disable=SC2013
	while read -r srcpath; do
		# Check for absolute path:
		case "$srcpath" in
			(''|'#'*) continue ;; # empty line or comment, skip it
			('~/'*) srcpath=$HOME${srcpath#'~'} ;;
			(/*) ;;
			(*) die $E_CONFIG '%s: not an absolute path' "$srcpath" ;;
		esac

		# Remove trailing slash:
		case "$srcpath" in (*/)
			test -d "$srcpath" || die $E_CONFIG '%s: Not a directory' "$srcpath"
			srcpath=${srcpath%/}
		esac

		# Avoid recursively copying the working directory itself:
		case "$WORKDIR" in ("$srcpath"*)
			die $E_CONFIG \
			    '%s: would recursively include working directory %s' \
			    "$srcpath" "$WORKDIR"
		esac

		# Create all directories leading up to the destination path:
		dstpath=$WORKDIR$srcpath
		mkdir -p "${dstpath%/*}"

		# Assemble rsync options:
		set -- --human-readable
		set -- "$@" --recursive --links --perms --times --owner --group
		set -- "$@" --sparse --hard-links --numeric-ids

		# Add trailing slash to source path if it's a directory (for rsync):
		if [ -d "$srcpath" ]; then
			srcpath=$srcpath/
		fi

		# Copy the file:
		rsync "$@" "$srcpath" "$dstpath" \
			|| die $E_FILESYSTEM
	done <"$LISTFILE"
}

pack_workdir()
(
	info 'Packing up working directory'

	cd "${WORKDIR%/*}"
	bsdtar -czf "$WORKDIR".tar.gz "${WORKDIR#$(pwd)/}" || die $E_FILESYSTEM
)

encrypt_workdir()
{
	info 'Encrypting tarball'

	if $FORCE; then
		warn 'Force-run: overwriting archive file %s' "$ARCHIVE"
		rm -f "$ARCHIVE"
	fi

	openssl aes-128-cbc -e -salt -pbkdf2 -in "$WORKDIR".tar.gz -out "$ARCHIVE" \
		|| die $E_ENCRYPTION
}

cleanup_workdir()
{
	info 'Cleaning up working directory'

	rm -rf "${WORKDIR%/*}"
}

unpack()
{
	mkdir "$ARCHIVE.d" \
		|| die $E_FILESYSTEM \
		       '%s: cannot create extraction directory' "$ARCHIVE.d"
	openssl aes-128-cbc -d -pbkdf2 -in "$ARCHIVE" \
		| bsdtar -xzf - -C "$ARCHIVE.d"
}

# UTILITY FUNCTIONS ============================================================

# Message printing utilities (debug messages are printed to stderr to avoid
# breaking functions that expect their output to be processed in any way):
error()   { printf '> \033[31mERROR\033[0m: '   >&2; printf "$@" >&2; echo >&2; }
warn()    { printf '> \033[33mWARNING\033[0m: ' >&2; printf "$@" >&2; echo >&2; }
info()    { printf '> '                            ; printf "$@"    ; echo    ; }
success() { printf '> \033[32mSUCCESS!\033[0m '    ; printf "$@"    ; echo    ; }
debug() {
	if $DEBUG; then
		printf '\033[34m> '
		printf "$@"
		printf '\033[0m\n'
	fi >&2
}

die()
{
	retval=$1; shift
	prefix=
	case $retval in
		("$E_USER") prefix='E_USER' ;;
		("$E_FILESYSTEM") prefix='E_FILESYSTEM' ;;
		("$E_ENV") prefix='E_ENV' ;;
		("$E_CONFIG") prefix='E_CONFIG' ;;
		("$E_ENCRYPTION") prefix='E_ENCRYPTION' ;;
		("$E_INTERNAL") prefix='E_INTERNAL' ;;
	esac
	if [ -n "$prefix" ]; then
		printf '\033[31m%s\033[0m' "$prefix" >&2
		if [ $# -gt 0 ]; then
			printf ': ' >&2
		else
			echo >&2
		fi
	fi
	if [ $# -gt 0 ]; then
		printf "$@" >&2; echo >&2
	fi
	if $needs_cleanup; then
		cleanup_workdir
	fi
	if [ $retval -eq $E_USER ]; then
		printf 'Run `%s -h` for usage information.\n' "$PROGRAM_NAME" >&2
	fi
	exit $retval
}

print_help()
{
	cat <<- EOF
	$PROGRAM_NAME - run a backup

	Usage: $PROGRAM_NAME -h
	       $PROGRAM_NAME create [-f] ARCHIVE
	       $PROGRAM_NAME extract ARCHIVE

	Actions:
	  create  Create a new backup ARCHIVE according to the configuration file
	  extract Extract data from a backup ARCHIVE to a directory

	Options:
	  -h      Display this help message and exit
	  -f      (create only): Overwrite ARCHIVE if it exists
	EOF
}

# MAIN =========================================================================

# Invocation:
readonly PROGRAM_NAME=cucumber

# Error codes:
readonly E_SUCCESS=0
readonly E_USER=1
readonly E_FILESYSTEM=2
readonly E_ENV=3
readonly E_CONFIG=4
readonly E_ENCRYPTION=5
readonly E_INTERNAL=13

# Options:
FORCE=false
while getopts :fh opt; do
	case "$opt" in
		(f) FORCE=true ;;
		(h) print_help; exit $E_SUCCESS ;;
		(:) die $E_USER 'Missing argument for -%s' "$OPTARG" ;;
		('?') die $E_USER 'Unknown option -%s' "$OPTARG" ;;
		(*) die $E_INTERNAL 'Unhandled option -%s' "$opt" ;;
	esac
done
readonly FORCE
shift $((OPTIND - 1))
OPTIND=1
unset OPTARG

# Positional arguments:
test $# -gt 0 || die $E_USER 'Missing action'
readonly ACTION=$1; shift
test $# -gt 0 || die $E_USER 'Missing archive file'
ARCHIVE=$1; shift

# Run backup (see also the 'Concept' section in the readme):
case "$ACTION" in
	(create)
		sanity_checks
		create_workdir
		copy_data
		pack_workdir
		encrypt_workdir
		cleanup_workdir
		;;
	(extract)
		unpack
		;;
	(*) die $E_USER 'Unknown action: %s' "$ACTION" ;;
esac
